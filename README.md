# CodingCurriculum

## CSunplugged

* https://www.csunplugged.org/de/

* https://www.youtube.com/watch?v=OkcVk_PGYL4

* https://www.youtube.com/watch?v=GfzuQfodg1w

## BYOB

* https://lightbot.com/flash.html

* https://blockly.games/

* https://snap.berkeley.edu/

* https://inf-schule.de/tools/24-it4kids-cubi

* https://algonautin.de/

* https://algo.bwinf.de/

## Python-Turtle

* https://webtigerpython.ethz.ch/

* https://python-online.ch/

* https://pythonsandbox.com/turtle

* https://inf-schule.de/tools/python

* https://inf-schule.de/tools/spacebug

* https://codecombat.com/

* https://jupyter.org/

* https://strype.org/

## Java 

* https://java-online.ch/

## apk

* https://appinventor.mit.edu/

* http://www.appinventortojava.com/login/

## OOP

* https://inf-schule.de/tools/klassendiagramm-interaktiv

## SVG

* https://inf-schule.de/tools/svg


## HTML/CSS/JS

* https://caniuse.com/

* https://www.w3.org/developers/tools/

* https://runjs.co/

* https://jsfiddle.net/

* https://inf-schule.de/tools/html

* https://apps.informatik.cc/html-css-js-editor/ 

* https://apps.informatik.cc/js-console/

* https://web.archive.org/

## TCP/IP/DHCP/DNS

* https://hack.arrrg.de/

* https://gsuite.tools/de/traceroute

* http://www.dnstools.ch/visual-traceroute.html

* https://geotraceroute.com/

* https://www.wieistmeineip.de/ 

* https://inf-schule.de/tools/ping

## SMTP

* https://anonbox.net/

## Logic

* https://inf-schule.de/tools/logic_tool

* https://inf-schule.de/tools/dsim

## Bin/Dec/Hex

* https://inf-schule.de/tools/zahlensysteme

* https://scratch.mit.edu/projects/227035991/ 

## Assembler

* https://inf-schule.de/tools/bonsai-assembler-interpreter

* https://inf-schule.de/tools/bonsai

* https://inf-schule.de/tools/bonsai_uebersetzer

## OS

* https://bellard.org/jslinux/

## XML

* https://inf-schule.de/tools/validator

## SQL

* https://www.mycompiler.io/new/sql

* https://sql-island.informatik.uni-kl.de/

* https://inf-schule.de/tools/DBAbfragen

* https://apps.informatik.cc/sql/sqlite.php

## ML

* https://quickdraw.withgoogle.com/

* https://www.i-am.ai/de/talk-neural-numbers.html

* https://www.soekia.ch/GPT/

* https://ddi-kiki.informatik.uni-erlangen.de/station11/static/html/SimulationVideo.html

* https://www.i-am.ai/apps/gradient-descent-vg/?lang=de

* https://imaginary.github.io/gradient-descent/?lang=de

* https://www.moralmachine.net/

* https://www.biastest.ch/

* https://www.aiunplugged.org/

* https://machinelearningforkids.co.uk/#!/signup

* https://computingeducation.de/proj-it2school/

* https://klassenkarte.de/unravel/

* https://imaginary.github.io/sumory/?lang=de

* https://orangedatamining.com/

* https://www.kiki-labor.fau.de/

* https://ncase.me/loopy/

* https://www.stefanseegerer.de/schlag-das-krokodil/?robots=true

* https://www.hartundtrocken.de/my-product/interaktiv-lineare-regression/

* https://www.hartundtrocken.de/interaktiv/

* https://www.hartundtrocken.de/my-product/interaktiv-naive-bayes-algorithmus/

* https://tm.gen-ai.fi/image/general

* https://www.cleverbot.com/

## Pixel

* https://inf-schule.de/tools/pixel

## RGB

* https://informatik.schule.de/rgb/RGB_farbmischer.html

## 3D

* https://www.tinkercad.com/

* https://beetleblocks.com/

* https://www.cospaces.io/

* https://computingeducation.de/proj-3d-art/

## Geo

* https://map.geo.admin.ch/#/map

* https://inf-schule.de/tools/gpx-viewer

## RSA

* https://apps.informatik.cc/rsa/

* https://www.hashgenerator.de/

## Data

* https://apps.informatik.cc/tracking/

* https://apps.informatik.cc/api-demo/

* https://www.bfs.admin.ch/bfs/de/home/statistiken/bevoelkerung/geburten-todesfaelle/vornamen-neugeborene.html

* https://pageviews.wmcloud.org/

* https://ourworldindata.org/

* https://www.datawrapper.de/

* https://de.venngage.com/

* https://www.tableau.com/

* https://infogram.com/

* https://codap.concord.org/

* https://annopy.de/

* https://apps.informatik.cc/sort/

## Robotics

* https://inf-schule.de/tools/zustandsmodellierung-mikrocontroller

* https://lab.open-roberta.org/

## LaTeX

* https://www.overleaf.com/

## QR-Codes

* https://www.youtube.com/watch?v=yiLjWBfQyF4

## Lit

* https://inf-schule.de/

* https://swisseduc.ch/informatik/

* https://www.abenteuer-informatik.de/schulmaterial.html

* https://dataliteracy.education/

* https://appcamps.de/

* https://schuelerlabor.informatik.rwth-aachen.de/

* https://003.lmvz.ch/connected/connected_1/1_1.html

* https://www.hopp-foundation.de/unterrichtsmaterial/zum-download/

* https://www.prodabi.de/

* https://ki-campus.org/

* https://ki-macht-schule.de/

* https://public.3.basecamp.com/p/tE7c2PXfA5eDD3MXX1YUtD7K

* https://gmls.phsz.ch/

* https://die-denkschule.ch/informatik/

* https://computingeducation.de/

* https://edulabs.de/oer/

* https://jugendhackt.org/oer/

* https://www.mi.fu-berlin.de/en/inf/groups/ag-ddi/index.html

* https://computingeducationresearch.org/














